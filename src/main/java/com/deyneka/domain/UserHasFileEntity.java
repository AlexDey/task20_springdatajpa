package com.deyneka.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_has_file", schema = "hibernate_test_db", catalog = "")
@IdClass(UserHasFileEntityPK.class)
public class UserHasFileEntity {
    private int userId;
    private int userPasswordId;
    private int fileId;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "user_password_id")
    public int getUserPasswordId() {
        return userPasswordId;
    }

    public void setUserPasswordId(int userPasswordId) {
        this.userPasswordId = userPasswordId;
    }

    @Id
    @Column(name = "file_id")
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserHasFileEntity that = (UserHasFileEntity) o;
        return userId == that.userId &&
                userPasswordId == that.userPasswordId &&
                fileId == that.fileId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userPasswordId, fileId);
    }
}
