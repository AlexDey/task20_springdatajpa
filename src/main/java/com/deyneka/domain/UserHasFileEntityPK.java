package com.deyneka.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class UserHasFileEntityPK implements Serializable {
    private int userId;
    private int userPasswordId;
    private int fileId;

    @Column(name = "user_id")
    @Id
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Column(name = "user_password_id")
    @Id
    public int getUserPasswordId() {
        return userPasswordId;
    }

    public void setUserPasswordId(int userPasswordId) {
        this.userPasswordId = userPasswordId;
    }

    @Column(name = "file_id")
    @Id
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserHasFileEntityPK that = (UserHasFileEntityPK) o;
        return userId == that.userId &&
                userPasswordId == that.userPasswordId &&
                fileId == that.fileId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userPasswordId, fileId);
    }
}
